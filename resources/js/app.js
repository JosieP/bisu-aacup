require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import moment from 'moment'
import copyText from "@meforma/vue-copy-to-clipboard";
import 'toastr2/dist/toastr.min.css';

// import Components
import BisuInput from "@/Components/Input";
import BisuInputSearch from "@/Components/InputSearch";
import BisuButton from "@/Components/Button";
import BisuLink from "@/Components/Link";
import BisuInputError from "@/Components/InputError";
import BisuDropdownLink from "@/Components/DropdownLink";
import BisuDropdown from "@/Components/Dropdown";
import BisuResponsiveNavLink from "@/Components/ResponsiveNavLink";
import BisuNavLink from "@/Components/NavLink";
import BisuModal from "@/Components/Modal";
import BisuSelect from "@/Components/Select";
import BisuCard from "@/Components/Card";
import BisuToggle from "@/Components/Toggle";
import BisuTextarea from "@/Components/Textarea";
import BisuTagsinput from "@/Components/Tagsinput";
import BisuCheckbox from "@/Components/Checkbox";
import ParameterSubLists from "./Pages/ParameterSubLists";

const el = document.getElementById('app');

createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({
        methods: {
            route,
            format_date(date) {
                if (date) {
                    return moment(String(date)).format('MMMM DD, YYYY')
                  }
            },
            integer_to_roman(num) {
                if (typeof num !== "number") return false;

                var digits = String(+num).split(""),
                    key = [
                        "",
                        "C",
                        "CC",
                        "CCC",
                        "CD",
                        "D",
                        "DC",
                        "DCC",
                        "DCCC",
                        "CM",
                        "",
                        "X",
                        "XX",
                        "XXX",
                        "XL",
                        "L",
                        "LX",
                        "LXX",
                        "LXXX",
                        "XC",
                        "",
                        "I",
                        "II",
                        "III",
                        "IV",
                        "V",
                        "VI",
                        "VII",
                        "VIII",
                        "IX",
                    ],
                    roman_num = "",
                    i = 3;
                while (i--)
                    roman_num = (key[+digits.pop() + i * 10] || "") + roman_num;
                return Array(+digits.join("") + 1).join("M") + roman_num;
            },
            integer_to_alpha(num){
                var s = '', t;
              
                while (num > 0) {
                  t = (num - 1) % 26;
                  s = String.fromCharCode(65 + t) + s;
                  num = (num - t)/26 | 0;
                }
                return s || undefined;
            },
            timeSince(date) {

                var seconds = Math.floor((new Date() - new Date(date)) / 1000);
                
                var interval = seconds / 31536000;
                
                if (interval > 1) {
                    return Math.floor(interval) + " y";
                }
                interval = seconds / 2592000;
                if (interval > 1) {
                    return Math.floor(interval) + " m";
                }
                interval = seconds / 86400;
                if (interval > 1) {
                    return Math.floor(interval) + " d";
                }
                interval = seconds / 3600;
                if (interval > 1) {
                    return Math.floor(interval) + " h";
                }
                interval = seconds / 60;
                if (interval > 1) {
                    return Math.floor(interval) + " min";
                }
                return Math.floor(seconds) + " sec";
            },
        },
      })
    .use(InertiaPlugin)
    .use(copyText)
    .component('BisuInput', BisuInput)
    .component('BisuInputSearch', BisuInputSearch)
    .component('BisuButton', BisuButton)
    .component('BisuLink', BisuLink)
    .component('BisuInputError', BisuInputError)
    .component('BisuDropdownLink', BisuDropdownLink)
    .component('BisuDropdown', BisuDropdown)
    .component('BisuResponsiveNavLink', BisuResponsiveNavLink)
    .component('BisuNavLink', BisuNavLink)
    .component('BisuModal', BisuModal)
    .component('BisuSelect', BisuSelect)
    .component('BisuCard', BisuCard)
    .component('BisuToggle', BisuToggle)
    .component('BisuTextarea', BisuTextarea)
    .component('BisuCheckbox', BisuCheckbox)
    .component('BisuTagsinput', BisuTagsinput)
    .component('ParameterSubLists', ParameterSubLists)
    .mount(el);

InertiaProgress.init({ color: '#4B5563' });
