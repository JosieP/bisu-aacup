## 🚀 Quick Start

```bash
# First, clone the project
upper right corner, click clone button and copy the link

# Then, locate the cloned project folder using gitbash and install dependencies
composer install

# Then, copy the environment file and generate key
cp .env.example .env
php artisan key:generate

# Then, configure your database on .env file
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<database name> ex.: bisu_bc
DB_USERNAME=<database username> default: root
DB_PASSWORD=<databse password> default: (leave it blank)

# Then, migrate table into database
php artisan migrate:refresh --seed

# Then, install node dependencies
npm install

# Finally, serve with hot reload at http://localhost:3000/
npm run watch
```

## Updating Project

```bash
# First, locate the cloned project folder using gitbash
git pull

# Then, migrate table into database
php artisan migrate:refresh --seed (optional)

# Then, install composer and node dependencies
npm install (optional)
composer install (optional)

# Finally, serve with hot reload at http://localhost:3000/
npm run watch
```

Note: running `php artisan migrate:refresh --seed` - remove all your data in the database and reset to default data.
