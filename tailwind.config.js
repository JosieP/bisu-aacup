const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
    purge: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
        "./resources/js/**/*.vue",
    ],

    theme: {
        extend: {
            colors: {
                primary: "#191847",
                secondary: "#01C1D8",
            },
            fontFamily: {
                sans: ["Poppins", ...defaultTheme.fontFamily.sans],
            },
            backgroundImage: (theme) => ({
                logo: "url('/images/bg.png')",
                cover: "url('/images/cover.jpg')",
                image: "url('/images/cover.jpg')",
            }),
        },
        listStyleType: {
            square: "square",
            roman: "upper-roman",
            alpha: "upper-alpha",
            decimal: "upper-alpha",
        },
    },

    variants: {
        extend: {
            opacity: ["disabled"],
            borderWidth: ["last", "first"],
        },
    },

    plugins: [require("@tailwindcss/forms")],
};
