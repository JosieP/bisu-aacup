<?php

use App\Http\Controllers\AreaController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DataBoxController;
use App\Http\Controllers\EfileController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\FolderController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ParameterController;
use App\Http\Controllers\ParameterListController;
use App\Http\Controllers\ProgramCertificateController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SummariesController;
use App\Http\Controllers\UploadFilesController;
use App\Http\Controllers\UserController;
use App\Models\Area;
use App\Models\Course;
use App\Models\Parameter;
use App\Models\ParameterList;
use App\Models\RatingSetting;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/dashboard', function () {
    return Inertia::render('Welcome');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/home', function () {
    return Inertia::render('Welcome');
})->middleware(['auth', 'verified'])->name('home');

require __DIR__ . '/auth.php';

Route::get('/register', function () {
    return redirect('/login');
})->name('register');

Route::get('/e-files-of-supporting-documents', [HomeController::class, 'e_files_of_supporting_documents'])->name('e_files.index');
Route::get('/databox', [DataBoxController::class, 'index']);
Route::get('/e-files-of-supporting-documents/{course}', [EfileController::class, 'show'])->name('e-file');
// Route::get('/courses/{course}', [EfileController::class, 'index'])->name('e-file.index');
Route::get('/accreditors', [HomeController::class, 'accreditors']);
Route::get('/contact-details-of-local-counterparts', [HomeController::class, 'contact_details']);
Route::get('/settings/{name}', [SettingController::class, 'index']);
Route::get('/summaries/comments', [SummariesController::class, 'comments']);
Route::get('/summaries/average', [SummariesController::class, 'tabulator']);
Route::get('/summaries/report/{course}', [SummariesController::class, 'summaryReport']);
Route::get('/summaries/comments/report/{feedback}', [SummariesController::class, 'summaryCommentReport']);
Route::get('/rating/{area}/area', [RatingController::class, 'index']);
Route::get('/print/{area}', [AreaController::class, 'print']);

Route::post('/upload-files', [UploadFilesController::class, 'store']);
Route::post('/upload-file/{parameterList}', [ParameterListController::class, 'store_file']);
Route::post('/save-rate', [ParameterListController::class, 'save_rate']);
Route::post('/temporary-save-rate', [ParameterListController::class, 'temporary_save_rate']);
Route::post('/submit-rate', [ParameterListController::class, 'submit_rate']);

Route::put('/settings/{user}', [SettingController::class, 'update_account']);
Route::put('/update-rating/{area}', [SettingController::class, 'update_rating']);
Route::put('/settings/update-password/{user}', [SettingController::class, 'updatePassword']);

Route::delete('/databox/{files}', [DataBoxController::class, 'destroy'])->name('databox.destroy');

Route::resource('/courses', CourseController::class);
Route::resource('/areas', AreaController::class);
Route::resource('/parameters', ParameterController::class);
Route::resource('/parameter-lists', ParameterListController::class);
Route::resource('/feedbacks', FeedbackController::class);
Route::resource('/users', UserController::class);
Route::resource('/programs', ProgramCertificateController::class);
Route::resource('/reports', ReportController::class);
Route::resource('/folders', FolderController::class);

Route::get('/reset-ratings', function () {
    Course::whereNotNull('rating')->update(['rating' => null]);
    Area::query()->update(['rating' => null, 'isRating' => false]);
    Parameter::whereNotNull('rating')->update(['rating' => null]);
    ParameterList::whereNotNull('rating')->update(['rating' => null]);
    RatingSetting::query()->delete();

    return response()->json('Done reset ratings');
});
