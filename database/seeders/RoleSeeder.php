<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "CREATING ADMIN: \n";

        Role::updateOrCreate([
            'name' => 'Super Admin'
        ]);

        Role::updateOrCreate([
            'name' => 'Admin'
        ]);

        Role::updateOrCreate([
            'name' => 'Accreditor'
        ]);

        echo "Done \n";
    }
}
