<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        echo "CREATING COURSES: \n";

        Course::updateOrCreate([
            'name' => 'Computer Science'
        ]);
        Course::updateOrCreate([
            'name' => 'Information Technology'
        ]);
        Course::updateOrCreate([
            'name' => 'Electrical Technology'
        ]);
        Course::updateOrCreate([
            'name' => 'Electronics Technology'
        ]);
        Course::updateOrCreate([
            'name' => 'Industrial Technology major in Food Technology'
        ]);

        echo "Done \n";
    }
}
