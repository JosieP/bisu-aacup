<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "CREATING SUPER ADMIN: \n";
        $user = User::firstOrNew([
            'email' => 'ivanlargo01@gmail.com'
        ]);
        $user->password = Hash::make('password');
        $user->suffix = 'Mr.';
        $user->firstname = 'Giovanni';
        $user->lastname = 'Largo';
        $user->contact_number = '09345678901';
        $user->gender = 'Male';
        $user->position = 'Student';
        $user->school = 'BISU';
        $user->assignRole('Super Admin');
        $user->save();

        $user = User::firstOrNew([
            'email' => 'bernangelot@gmail.com'
        ]);
        $user->password = Hash::make('password');
        $user->suffix = 'Mr.';
        $user->firstname = 'Bern Angelo';
        $user->lastname = 'Templa';
        $user->contact_number = '09345678901';
        $user->gender = 'Male';
        $user->position = 'Student';
        $user->school = 'BISU';
        $user->assignRole('Admin');
        $user->save();

        $user = User::firstOrNew([
            'email' => 'kheyciebanal@gmail.com'
        ]);
        $user->password = Hash::make('password');
        $user->suffix = 'Ms.';
        $user->firstname = 'Kheycie';
        $user->lastname = 'Banal';
        $user->contact_number = '09345678901';
        $user->gender = 'Female';
        $user->position = 'Student';
        $user->school = 'BISU';
        $user->assignRole('Accreditor');
        $user->save();

        echo "Done \n";
    }
}
