<?php

use App\Models\Feedback;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAreaIdToFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feedback', function (Blueprint $table) {

          Feedback::query()->delete();
          
          $table->dropForeign(['parameter_id']);
          $table->dropColumn('parameter_id');
          $table->foreignId('area_id')->after('id')->constrained('areas')->onDelete('cascade');
          $table->text('improved')->nullable()->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
          $table->dropForeign(['area_id']);
            $table->dropColumn('area_id');
            $table->foreignId('parameter_id')->after('id')->constrained('parameters')->onDelete('cascade');
            $table->dropColumn('improved');
        });
    }
}
