<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParameterListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_lists', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parameter_id')->constrained('parameters')->onDelete('cascade');
            $table->string('name');
            $table->foreignId('sub_parameter')->nullable()->constrained('parameter_lists')->onDelete('cascade');
            $table->foreignId('user_uploader')->nullable()->constrained('users')->onDelete("set null");
            $table->boolean('isUpload')->default(false);
            $table->string('filename')->nullable();
            $table->string('url')->nullable();
            $table->float('rating', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_lists');
    }
}
