<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParameterListFile extends Model
{
    use HasFactory;

    protected $fillable = ['parameter_list_id', 'file_id'];
}
