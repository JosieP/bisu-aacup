<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'area', 'course_id', 'accreditor_id', 'faculty_id', 'rating', 'isRating', 'weight'
    ];

    protected $casts = [
        'isRating' => 'boolean'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class)->with('user');
    }

    public function rating_setting()
    {
        return $this->hasOne(RatingSetting::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_areas')->with('roles');
    }

    public function accreditor()
    {
        return $this->belongsTo(User::class, 'accreditor_id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'faculty_id');
    }

    public function parameters()
    {
        return $this->hasMany(Parameter::class)->with('parameter_lists', function ($sub) {
            $sub->where('sub_parameter', null);
        });
    }
    public function report()
    {
        return $this->hasOne(Report::class);
    }
}
