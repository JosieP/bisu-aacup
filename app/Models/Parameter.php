<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'area_id', 'rating'];

    public function area()
    {
        return $this->belongsTo(Area::class)->with('course');
    }
    public function parameter_lists()
    {
        return $this->hasMany(ParameterList::class)->with('sub_parameters')->with('files');
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class)->with('user');
    }
}
