<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    use HasFactory;

    protected $fillable = ['filename', 'url', 'is_link', 'file_folder_id'];

    public function folder() {
      return $this->belongsTo(FileFolder::class, 'file_folder_id', 'id');
    }
}
