<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParameterList extends Model
{
    use HasFactory;

    protected $fillable = ['parameter_id', 'name', 'sub_parameter', 'user_uploader', 'isUpload', 'filename', 'url', 'rating'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'isUpload' => 'boolean',
    ];

    public function parameters()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function files()
    {
        return $this->belongsToMany(Files::class, 'parameter_list_files', 'parameter_list_id', 'file_id');
    }

    public function sub_parameters()
    {
        return $this->hasMany(ParameterList::class, 'sub_parameter')->with('sub_parameters')->with('files');
    }

    public function parameter_list()
    {
        return $this->belongsTo(ParameterList::class, 'sub_parameter');
    }

    public function param()
    {
        return $this->belongsTo(Parameter::class, 'parameter_id')->with('area');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'user_uploader');
    }
}
