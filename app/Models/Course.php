<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    // public function areas()
    // {
    //     return $this->hasMany(Area::class)->with('feedbacks')->with('parameters')->with('rating_setting')->with('report')->with('users')->orderBy('area');
    // }

    public function areas()
    {
        return $this->hasMany(Area::class)->orderBy('area');
    }

    public function hasNoAccreditorAreas()
    {
        return $this->hasMany(Area::class)->where('accreditor_id', null)->orderBy('area');
    }
    public function hasNoAdminAreas()
    {
        return $this->hasMany(Area::class)->where('faculty_id', null)->orderBy('area');
    }

    public function accreditor_areas()
    {
        return $this->hasMany(Area::class)->with('users');
    }

    public function admin_areas()
    {
        return $this->hasMany(Area::class)->with('users');
    }
}
