<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'suffix',
        'firstname',
        'lastname',
        'email',
        'contact_number',
        'gender',
        'position',
        'password',
        'school'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function parameter_lists()
    {
        return $this->hasMany(ParameterList::class, 'user_uploader')->with('param');
    }

    public function accreditorAreas()
    {
        return $this->hasMany(Area::class, 'accreditor_id')->with('course');
    }

    public function adminAreas()
    {
        return $this->hasMany(Area::class, 'faculty_id');
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class, 'user_areas');
    }
}
