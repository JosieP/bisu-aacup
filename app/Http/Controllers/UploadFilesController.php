<?php

namespace App\Http\Controllers;

use App\Models\FileFolder;
use App\Models\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class UploadFilesController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
          'link' => "required_without:files|max:10000",
          'files' => "required_without:link|max:10000",
            'folder' => ['required']
        ]);
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $contents = file_get_contents($file);
                $filename = $file->getClientOriginalName();
                $url = str_replace(' ', '-', strtolower($filename));
                $folder_name = FileFolder::find($request->input('folder'))->name;
                $path = 'files/' . $folder_name . '/' . $url;

                Storage::disk('public')->put($path, $contents);

                Files::create([
                    'filename' => $filename,
                    'is_link' => 0,
                    'url' => $path,
                    'file_folder_id' => $request->input('folder')
                ]);
            }
        } else {
          Files::create([
            'filename' => 'link-' . now(),
            'is_link' => 1,
            'url' => $request->input('link'),
            'file_folder_id' => $request->input('folder')
        ]);
        }


        return redirect()->back()
            ->with('message', 'Files successfully uploaded.');
    }
}
