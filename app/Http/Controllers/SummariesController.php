<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Feedback;
use Inertia\Inertia;

class SummariesController extends Controller
{
    public function comments()
    {
        return Inertia::render('summaries/Comments', [
            'comments' => Feedback::with('user')->with('area')->orderBy('created_at', 'DESC')->get()
        ]);
    }

    public function tabulator()
    {
        return Inertia::render('summaries/Tabulator', [
            'course_areas' => Course::with('areas')->get()
        ]);
    }


    public function summaryReport(Course $course)
    {
        return Inertia::render('summaries/Report', [
            'areas' => $course->areas()->get()
        ]);
    }

    public function summaryCommentReport(Feedback $feedback)
    {
        return Inertia::render('summaries/CommentReport', [
            'feedback' => $feedback->load('area')
        ]);
    }
}
