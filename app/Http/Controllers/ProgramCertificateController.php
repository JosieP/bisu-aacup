<?php

namespace App\Http\Controllers;

use App\Models\ProgramCertificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class ProgramCertificateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Program', [
            'programs' => ProgramCertificate::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->file('files') as $file) {
            $program = new ProgramCertificate();
            $contents = file_get_contents($file);
            $program->url = str_replace(' ', '-', strtolower($file->getClientOriginalName()));
            $program->filename = $file->getClientOriginalName();
            $path = 'programs/' . $program->url;
            Storage::disk('public')->put($path, $contents);
            $program->save();
        }

        return redirect()->back()
            ->with('message', 'File(s) successfully uploaded.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgramCertificate  $programCertificate
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramCertificate $programCertificate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgramCertificate  $programCertificate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramCertificate $programCertificate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgramCertificate  $programCertificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramCertificate $programCertificate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgramCertificate  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramCertificate $program)
    {
        $program->delete();
        Storage::disk('public')->delete('programs/' . $program->url);

        return redirect()->back()
            ->with('message', $program->filename . ' successfully deleted.');
    }
}
