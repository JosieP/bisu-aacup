<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Course;
use App\Models\FileFolder;
use App\Models\Files;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class EfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course, Area $area)
    {
        $areas = [
            [
                "id" => 1,
                "name" => "I"
            ],
            [
                "id" => 2,
                "name" => "II"
            ],
            [
                "id" => 3,
                "name" => "III"
            ],
            [
                "id" => 4,
                "name" => "IV"
            ],
            [
                "id" => 5,
                "name" => "V"
            ],
            [
                "id" => 6,
                "name" => "VI"
            ],
            [
                "id" => 7,
                "name" => "VII"
            ],
            [
                "id" => 8,
                "name" => "VIII"
            ],
            [
                "id" => 9,
                "name" => "IX"
            ],
            [
                "id" => 10,
                "name" => "X"
            ],

        ];
        $accreditors = [];
        foreach (Role::where('name', 'Accreditor')->first()->users as $accreditor) {
            array_push($accreditors, [
                'id' => $accreditor->id,
                'name' => $accreditor->firstname . ' ' . $accreditor->lastname
            ]);
        }

        $admins = [];
        foreach (Role::where('name', 'Admin')->first()->users as $admin) {
            array_push($admins, [
                'id' => $admin->id,
                'name' => $admin->firstname . ' ' . $admin->lastname
            ]);
        }

        $course->load('areas');
        $area->load(['feedbacks', 'parameters', 'rating_setting', 'report', 'users']);

        // return $area;
        return Inertia::render('CourseFile', [
            'course' => $course,
            'selectedArea' => $area,
            'areas' => $areas,
            'accreditors' => $accreditors,
            'admins' => $admins,
            'files' => Files::with('folder')->orderBy('id', 'DESC')->get(),
            'folders' => FileFolder::orderBy('id', 'DESC')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        // , "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
