<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Users/Index', [
            'users' => User::with('roles')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Users/Create', [
            'courses' => Course::with('areas')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'suffix' => 'required|min:2',
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'position' => 'required|min:2',
            'email' => 'required',
            'contact_number' => 'required|min:5',
            'gender' => 'required|min:2',
            'school' => 'required',
        ]);

        $user = User::create([
            'suffix' => $request->input('suffix'),
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'position' => $request->input('position'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'contact_number' => $request->input('contact_number'),
            'gender' => $request->input('gender'),
            'school' => $request->input('school'),
        ]);
        $user->assignRole(Role::find($request->input('role')));

        return redirect()->back()->with('message', 'New user added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        return Inertia::render('Users/Edit', [
            'user' => $user->with('roles')->with('accreditorAreas')->with('adminAreas')->with('areas')->find($user->id),
            'courses' => Course::with('areas')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'suffix' => 'required|min:2',
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'position' => 'required|min:2',
            'email' => 'required',
            'contact_number' => 'required|min:5',
            'gender' => 'required|min:2',
            'school' => 'required',
        ]);

        $user->suffix = $request->input('suffix');
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->position = $request->input('position');
        $user->email = $request->input('email');
        $user->contact_number = $request->input('contact_number');
        $user->gender = $request->input('gender');
        $user->school = $request->input('school');

        $role = Role::find($request->input('role'));
        $user->syncRoles($role);
        $user->save();

        return redirect()->back()->with('message', 'User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->removeRole($user->roles[0]);
            $user->delete();

            return redirect()->back()->with('message', 'User successfully deleted.');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function update_account(Request $request, User $user)
    {
        $request->validate([
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'email' => 'required|min:2',
            'contact_number' => 'required|min:5',
            'gender' => 'required|min:2',
            'position' => 'required|min:2',
        ]);
        $user->update($request->all());

        return redirect()->back()
            ->with('message', 'Account updated.');
    }
}
