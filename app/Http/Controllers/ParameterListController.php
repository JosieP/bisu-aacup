<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Parameter;
use App\Models\ParameterList;
use App\Models\RatingSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ParameterListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        ParameterList::create($request->all());
        return redirect()->back()->with('message', 'New parameter list added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ParameterList  $parameterList
     * @return \Illuminate\Http\Response
     */
    public function show(ParameterList $parameterList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ParameterList  $parameterList
     * @return \Illuminate\Http\Response
     */
    public function edit(ParameterList $parameterList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ParameterList  $parameterList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParameterList $parameterList)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $parameterList->update($request->all());

        return redirect()->back()->with('message',  $parameterList->name . ' parameter list updated.');
    }

    public function save_rating($subs, $list)
    {
        $total = null;
        if (count($subs) !== 0) {
            $count = null;
            foreach ($subs as $sub) {
                $count = $count + $sub->rating;
            }
            $total = $count / count($subs);
        }

        $list->parameter_list()->update(['rating' => $total]);

        if ($list->parameter_list !== null) {
            $sub1 = ParameterList::where('sub_parameter', $list->parameter_list->sub_parameter)->first();
            $subs1 = ParameterList::where('sub_parameter', $list->parameter_list->sub_parameter)->where('rating', '<>', null)->where('rating', '>=', 0)->get();
            $this->save_rating($subs1, $sub1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ParameterList  $parameterList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParameterList $parameterList)
    {
        $parameterList->delete();

        return redirect()->back()
            ->with('message', $parameterList->name . ' successfully deleted.');
    }

    public function store_file(Request $request, ParameterList $parameterList)
    {
        $request->validate([
            'files' => "required|max:10000",
        ]);

        $parameterList->files()->sync($request->input('files'));
        // if ($request->hasFile('filename')) {
        //     $contents = file_get_contents($request->file('filename'));
        //     $parameterList->user_uploader = $request->input('user_uploader');
        //     $parameterList->url = str_replace(' ', '-', strtolower($request->file('filename')->getClientOriginalName()));
        //     $parameterList->filename = $request->file('filename')->getClientOriginalName();
        //     $path = 'files/' . $parameterList->url;
        //     Storage::disk('public')->put($path, $contents);
        //     $parameterList->save();
        // } else {
        //     $parameterList->user_uploader = null;
        //     $parameterList->url = null;
        //     $parameterList->filename = null;

        //     $parameterList->save();
        // }


        return redirect()->back()
            ->with('message', 'File successfully uploaded.');
    }

    public function temporary_save_rate(Request $request)
    {
        foreach ($request->ratings as $value) {
            ParameterList::where('id', $value['id'])->update(['rating' => $value['rating']]);
        }

        return redirect()->back()
            ->with('message', ' The rating was successfully temporary saved.');
    }

    public function submit_rate(Request $request)
    {
        $status = RatingSetting::where('area_id', $request->area_id)->first();
        $message = 'Please calculate first the rating before submitted.';
        $response = false;

        if ($status->status === 'computed') {
            $status->update(['status' => 'rated']);
            $message = 'The rating was successfully submitted.';
            $response = true;
        }

        if ($status->status === 'rated') {
            $status->update(['status' => 'rated']);
            $message = 'The rating was already submitted.';
            $response = true;
        }

        return redirect()->back()
            ->with('message', $message)
            ->with('response', $response);
    }

    public function save_rate(Request $request)
    {
        foreach ($request->ratings as $value) {
            ParameterList::where('id', $value['id'])->update(['rating' => round($value['rating'], 2)]);
            $this->calculate_rating(ParameterList::find($value['id']));
        }

        RatingSetting::updateOrCreate([
            'area_id' => $request->area_id
        ], [
            'status' => 'computed'
        ]);

        return redirect()->back()
            ->with('message', ' The rating was successfully saved.');
    }

    public function calculate_rating($parameterList)
    {
        $sub = ParameterList::where('sub_parameter', $parameterList->sub_parameter)->first();
        $subs = ParameterList::where('sub_parameter', $parameterList->sub_parameter)->where('rating', '<>', null)->where('rating', '>=', 0)->get();
        $this->save_rating($subs, $parameterList);

        $param = ParameterList::where('sub_parameter', null)->where('parameter_id', $parameterList->parameter_id)->first();
        $params = ParameterList::where('sub_parameter', null)->where('parameter_id', $parameterList->parameter_id)->where('rating', '<>', null)->where('rating', '>=', 0)->get();

        $total = null;
        if (count($params) !== 0) {
            $count = null;
            foreach ($params as $sub) {
                $count = $count + $sub->rating;
            }
            $total = $count / count($params);
        }

        $param->param()->update(['rating' => round($total, 2)]);

        $area = Parameter::where('area_id', $param->param->area_id)->first();
        $areas = Parameter::where('area_id', $param->param->area_id)->where('rating', '<>', null)->where('rating', '>=', 0)->get();

        $total1 = null;
        if (count($areas) !== 0) {
            $count = null;
            foreach ($areas as $sub) {
                $count = $count + $sub->rating;
            }
            $total1 = $count / count($areas);
        }
        $area->area()->update(['rating' => round($total1, 2)]);

        $course = Area::where('course_id', $area->area->course_id)->first();
        $courses = Area::where('course_id', $area->area->course_id)->where('rating', '<>', null)->where('rating', '>=', 0)->get();

        $total2 = null;
        if (count($courses) !== 0) {
            $count = null;
            foreach ($courses as $sub) {
                $count = $count + $sub->rating;
            }
            $total2 = $count / count($courses);
        }
        $course->course()->update(['rating' => round($total2, 2)]);
    }
}
