<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Course;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($name)
    {
        return Inertia::render('settings/Index', [
            'user' => Auth::user(),
            'couse_areas' => Area::with('course')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }

    public function update_account(Request $request, User $user)
    {
        $request->validate([
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'email' => 'required|min:2',
            'contact_number' => 'required|min:5',
            'gender' => 'required|min:2',
            'position' => 'required|min:2',
        ]);
        $user->update($request->all());

        return redirect()->back()
            ->with('message', 'Account updated.');
    }

    public function updatePassword(Request $request, User $user)
    {
        $request->validate([
            'currentPassword' => ['required'],
            'newPassword' => ['required', 'string', 'min:8', 'different:currentPassword'],
            'confirmPassword' => ['required', 'same:newPassword']
        ]);

        if (!Hash::check($request->input('currentPassword'), $user->password)) {
            return redirect()->back()
                ->with('error', 'Current password does not match.');
        }

        $user->update([
            'password' => Hash::make($request->input('newPassword')),
        ]);

        return redirect()->back()
            ->with('message', 'User Password updated.');
    }

    public function update_rating(Request $request, Area $area)
    {
        try {
            $request->validate([
                'isRating' => 'required',
            ]);

            $area->update($request->all());
            return redirect()->back()->with('message', 'Enable rating successfully.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('message', $th);
        }
    }
}
