<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'area' => 'required',
            'course_id' => 'required',
            'name' => 'required',
            'weight' => 'required'
        ]);
        try {
            $areas = Area::where('course_id', $request->input('course_id'))->pluck('area');
            foreach ($areas as $area) {
                if ((int)$area === (int)$request->input('area')) {
                    return redirect()->back()->with('error', 'The selected area is already exist.');
                }
            }
            $area = Area::create($request->except('users'));
            $area->users()->sync($request->input('users'));
            return redirect()->back()->with('message', 'New area added.');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        $request->validate([
            'area' => 'required',
            'name' => 'required',
            'weight' => 'required'
        ]);
        try {

            $area->update($request->all());
            $area->users()->sync($request->input('users'));
            return redirect()->back()->with('message', 'Update area successfully.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('message', $th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();

        return Redirect::route('courses.show', $area->course->id)
            ->with('message', $area->name . ' successfully deleted.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function print(Area $area)
    {
        return Inertia::render('PrintableArea', [
            'area' => Area::with('feedbacks')->with('parameters')->with('rating_setting')->with('report')->with('users')->find($area->id),
        ]);
    }
}
