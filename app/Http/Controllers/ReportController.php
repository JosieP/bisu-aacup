<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Report', [
            'courses' => Course::with('areas')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'filename' => "required|mimes:pdf,doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,mp4,mov|max:20000",
        ]);
        $report = Report::firstOrNew([
            'area_id' => $request->input('area_id')
        ]);
        if ($request->hasFile('filename')) {
            $contents = file_get_contents($request->file('filename'));
            $report->url = str_replace(' ', '-', strtolower($request->file('filename')->getClientOriginalName()));
            $report->filename = $request->file('filename')->getClientOriginalName();
            $path = 'reports/' . $report->url;
            Storage::disk('public')->put($path, $contents);
            $report->save();
        }



        return redirect()->back()
            ->with('message', 'Report file successfully uploaded.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->delete();

        return redirect()->back()
            ->with('message', $report->filename . ' was successfully deleted.');
    }
}
