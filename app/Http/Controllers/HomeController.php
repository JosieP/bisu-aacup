<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return Inertia::render('Auth/Login');
        }
        return Inertia::render('Welcome');
    }

    public function e_files_of_supporting_documents()
    {
        return Inertia::render('CourseLists', [
            'courses' => Course::with('areas')->get(),
        ]);
    }

    public function accreditors()
    {
        return Inertia::render('Accreditors', [
            'courses' => Course::with('accreditor_areas')->get(),
        ]);
    }

    public function contact_details()
    {
        return Inertia::render('ContactDetails', [
            'courses' => Course::with('admin_areas')->get(),
        ]);
    }
}
