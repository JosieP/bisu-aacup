<?php

namespace App\Http\Controllers;

use App\Models\FileFolder;
use App\Models\Files;
use App\Models\ParameterList;
use App\Models\ProgramCertificate;
use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;

class DataBoxController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return Inertia::render('Databox', [
            'parameter_lists' => ParameterList::with('param')->with('files')->get(),
            'programs' => ProgramCertificate::all(),
            'files' => Files::with('folder')->orderBy('id', 'DESC')->get(),
            'folders' => FileFolder::orderBy('id', 'DESC')->get(),
        ]);
    }

    public function destroy(Files $files)
    {
        $files->delete();
        return redirect()->back()
            ->with('message', 'File deleted.');
    }
}
