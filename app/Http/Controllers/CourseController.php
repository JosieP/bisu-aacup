<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Course;
use App\Models\FileFolder;
use App\Models\Files;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|unique:courses,name',
        ]);
        Course::create($request->all());
        return redirect()->back()->with('message', 'New course added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Course $course)
    {

        $areas = [
            [
                "id" => 1,
                "name" => "I"
            ],
            [
                "id" => 2,
                "name" => "II"
            ],
            [
                "id" => 3,
                "name" => "III"
            ],
            [
                "id" => 4,
                "name" => "IV"
            ],
            [
                "id" => 5,
                "name" => "V"
            ],
            [
                "id" => 6,
                "name" => "VI"
            ],
            [
                "id" => 7,
                "name" => "VII"
            ],
            [
                "id" => 8,
                "name" => "VIII"
            ],
            [
                "id" => 9,
                "name" => "IX"
            ],
            [
                "id" => 10,
                "name" => "X"
            ],

        ];
        $accreditors = [];
        foreach (Role::where('name', 'Accreditor')->first()->users as $accreditor) {
            array_push($accreditors, [
                'id' => $accreditor->id,
                'name' => $accreditor->firstname . ' ' . $accreditor->lastname
            ]);
        }

        $admins = [];
        foreach (Role::where('name', 'Admin')->first()->users as $admin) {
            array_push($admins, [
                'id' => $admin->id,
                'name' => $admin->firstname . ' ' . $admin->lastname
            ]);
        }

        $course->load('areas');
        $area = $request->input('area_id') ? Area::find($request->input('area_id')) : (count($course->areas) ? $course->areas[0] : '');

        if ($area) {
            $area->load(['feedbacks', 'parameters', 'rating_setting', 'report', 'users']);
        }

        // return $area;
        return Inertia::render('CourseFile', [
            'course' => $course,
            'selectedArea' => $area,
            'areas' => $areas,
            'accreditors' => $accreditors,
            'admins' => $admins,
            'files' => Files::with('folder')->orderBy('id', 'DESC')->get(),
            'folders' => FileFolder::orderBy('id', 'DESC')->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $request->validate([
            'name' => ['required', 'min:2', Rule::unique('courses')->ignore($course->id)],
        ]);
        $course->update($request->all());

        return redirect()->back()
            ->with('message', 'Course updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();

        return redirect()->back()
            ->with('message', 'Course deleted.');
    }
}
