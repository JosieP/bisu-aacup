<?php

namespace App\Http\Controllers;

use App\Models\FileFolder;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'name' => ['required'],
      ]);
      FileFolder::create($request->all());
      return redirect()->back()->with('message', 'New course added.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FileFolder  $folder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileFolder $folder)
    {
      $request->validate([
        'name' => ['required'],
    ]);
    $folder->update($request->all());

    return redirect()->back()
        ->with('message', 'Folder updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FileFolder  $folder
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileFolder $folder)
    {
      $folder->delete();

      return redirect()->back()
          ->with('message', 'Folder deleted.');
    }
}
